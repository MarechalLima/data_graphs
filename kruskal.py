import networkx as nx
import time
import sys
from memory_profiler import memory_usage

sys.setrecursionlimit(100000)

def find(pai, a):
    if(pai[a] == a):
        return a
    pai[a] = find(pai, pai[a])
    return pai[a]

def join(pai, a, b):
    fa = find(pai, a); fb=find(pai, b)
    pai[fa]=fb

#grafo como lista de arestas
def kruskal(graph, n):
    edges= sorted(graph.edges(data=True), key=lambda t: t[2].get('weight', 1))

    mst = nx.Graph()
    pai = [i for i in range(0,n+1)]
    cost = 0

    for edge in edges:
        u,v,w = edge
        w = w.get('w')

        if(find(pai, u) != find(pai, v)):
            cost += w

            mst.add_edge(u,v, weight=w)
            #mst.append(edge)
            join(pai, u,v)
    
    #print(cost)
    return cost

def run(filename):
    graph = nx.read_edgelist(filename, nodetype=int)
    n = len(graph)

    start = time.time()
    mem_usage = memory_usage((kruskal, [graph, n]))
    end = time.time()
    
    print(f'%.6fs'%(end-start), end=',')
    print(f'%.2fMB'%max(mem_usage))
    

    #print((end-start), mem_usage)

_,filename,i,p,n = sys.argv
print('kruskal',i,p,n,sep=',',end=',')
run(filename)

# n_var = [ 500, 1000, 5000]
# #n_var = [5000]
# probab = [0.3, 0.6, 0.9, 1]


# for n in n_var:
#     for p in probab:
#         for i in range(2,11):
#             print(f'Ensaio %d: %d vértices (d=%.1f)'%(i,n,p))
#             gc.collect()
#             run("graphs/" + str(i) + "_graph_p_" + str(p) + "_n_" + str(n) + ".yaml")

#run()