import pingouin as pg
import pandas as pd


def anova_tempo():
  df = pd.read_csv("result.csv")
  df.astype({'tempo': 'float32'}).dtypes

  print(df["tempo"].dtype.kind not in 'fi')

  aov = pg.anova(data=df, dv='tempo', between=['algoritmo', 'vertice', 'densidade'],
            effsize="n2").round(3)

  print(aov)

def anova_memoria():
  df = pd.read_csv("result.csv")
  df.astype({'memoria': 'float32'}).dtypes

  print(df["memoria"].dtype.kind not in 'fi')

  aov = pg.anova(data=df, dv='memoria', between=['algoritmo', 'vertice', 'densidade'],
            effsize="n2").round(3)

  print(aov)

print("============ ANOVA - MÉTRICA=TEMPO ==============")
anova_tempo()

print("\n\n============ ANOVA - MÉTRICA=MEMÓRIA ==============")
anova_memoria()