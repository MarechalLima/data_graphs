import queue as Q
import time
import sys
from memory_profiler import memory_usage
import networkx as nx

INF = 999999999

#S -> source node
#graph -> adjacency list
def prim(graph, n):
    distance = []
    used = []
    mst = []

    for i in range(0,n+1):
        distance.append(INF)
        used.append(False)
        mst.append(-1)
    distance[1]=0

    queue = Q.PriorityQueue()
    queue.put((0,1))

    while not queue.empty():
        u = queue.get()[1]

        used[u]=True
        
        adj = graph.adj[u]
        for v in adj:
            w = adj[v]['w']

            if(not used[v] and distance[v] > w):
                distance[v]=w
                mst[v]=(u,v,w)
                queue.put((w,v))

    cost = 0

    result = nx.Graph()

    for i in range(0,n):
        cost += distance[i]

        if(mst[i] != -1):
            u,v,w = mst[i]
            result.add_edge(u,v,weight=w)
        #print(mst[i])

    return result


def run(filename):
    graph = nx.read_edgelist(filename, nodetype=int)
    n = len(graph)

    start = time.time()
    mem_usage = memory_usage((prim, [graph, n]))
    end = time.time()
    
    print(f'%.6fs'%(end-start), end=',')
    print(f'%.2fMB'%max(mem_usage))
   

_,filename,i,p,n = sys.argv
print('prim',i,p,n,sep=',',end=',')
run(filename)
