#!/usr/bin/python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itertools

# 'tempo' in seconds
# 'memoria' in megabytes

data = pd.read_csv("data/result.csv", dtype={
    'ensaio': np.float64, 'densidade': np.float64,
    'tempo': np.float64, 'memoria': np.float64
})

dataBox = data.groupby(['algoritmo'])
print(dataBox.describe()['tempo'])
print(dataBox.describe()['memoria'])
