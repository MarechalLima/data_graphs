import os

n_var = [250, 500, 1000, 5000]
probab = [0.3, 0.6, 0.9, 1]


def run_tests(algorithm):
    for p in probab:
        for n in n_var:
            for i in range(1,11):
                filename = "graphs/graph_i_" + str(i) + "_p_" + str(p) + "_n_" + str(n) + ".txt"
                #filename = "graphs/" + str(i) + "_graph_p_" + str(p) + "_n_" + str(n) + ".txt"
                #print(algorithm,i,p,n,end='')

                args = f'%s %s %s %s'%(filename, i, p, n)

                os.system("python3 " + algorithm + ".py " + args)


run_tests('kruskal')
run_tests('prim')